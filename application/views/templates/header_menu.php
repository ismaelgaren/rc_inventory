
<header class="main-header">

    <!-- Logo -->
    <a href="https://rcinventory.000webhostapp.com/dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="<?php echo base_url('assets/images/icon.jpg') ?>" height="100%" width="100%" alt=""></span>
      <!-- logo for regular state and mobile devices -->
      <img src="<?php echo base_url('assets/images/logo.jpg') ?>" alt="" height="100%">
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

    </nav> 
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  